/*
 * Copyright (c) 2015 eneim. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package im.ene.lab.dowy.internal;

import okio.BufferedSink;
import okio.Okio;
import okio.Source;

import java.io.Closeable;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

/**
 * Created by eneim on 11/27/15.
 */
class FileUtil {

  static void joinFiles(File destination, File[] sources)
      throws IOException {
    BufferedSink buffer = Okio.buffer(Okio.appendingSink(destination));
    try {
      for (File file : sources) {
        Source source = Okio.source(file);
        buffer.writeAll(source);
        IOUtils.closeQuietly(source);
        file.delete();
      }
    } finally {
      IOUtils.closeQuietly(buffer);
    }
  }

  static void saveStreamToFile(File file, InputStream inputStream)
      throws FileNotFoundException {
    Source source = Okio.source(inputStream);
    BufferedSink des = Okio.buffer(Okio.sink(file));
    try {
      des.writeAll(source);
    } catch (IOException e) {
      e.printStackTrace();
    } finally {
      FileUtil.IOUtils.closeQuietly(des);
      FileUtil.IOUtils.closeQuietly(source);
    }
  }

  static void joinFiles(File destination, ArrayList<File> sources)
      throws IOException {
    BufferedSink buffer = Okio.buffer(Okio.appendingSink(destination));
    try {
      while (sources.size() > 0) {
        File file = sources.remove(0);
        Source source = Okio.source(file);
        buffer.writeAll(source);
        IOUtils.closeQuietly(source);
        file.delete();
      }
    } finally {
      IOUtils.closeQuietly(buffer);
    }
  }

  static void copyFiles(File from, File to) {
    try {
      Source a = Okio.source(from);
      BufferedSink b = Okio.buffer(Okio.sink(to));
      b.writeAll(a);
      IOUtils.closeQuietly(a);
      IOUtils.closeQuietly(b);
    } catch (IOException er) {
      er.printStackTrace();
    }
  }

  static class IOUtils {
    public static void closeQuietly(Closeable output) {
      try {
        if (output != null) {
          output.close();
        }
      } catch (IOException ioe) {
        ioe.printStackTrace();
      }
    }
  }

  static class IOCopier {
    public static void joinFiles(File destination, File[] sources)
        throws IOException {
      BufferedSink buffer = Okio.buffer(Okio.appendingSink(destination));
      try {
        for (File file : sources) {
          Source source = Okio.source(file);
          buffer.writeAll(source);
          IOUtils.closeQuietly(source);
        }
      } finally {
        IOUtils.closeQuietly(buffer);
      }
    }
  }
}
