/*
 * Copyright (c) 2015 eneim. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package im.ene.lab.dowy.internal;

import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.ResponseBody;

import im.ene.lab.dowy.OnProgressListener;
import okio.Buffer;
import okio.BufferedSource;
import okio.ForwardingSource;
import okio.Okio;
import okio.Source;

import java.io.IOException;

/**
 * Created by eneim on 11/27/15.
 */
class ProgressResponseBody extends ResponseBody {

  private final ResponseBody responseBody;
  private final OnProgressListener onProgressListener;
  private BufferedSource bufferedSource;

  public ProgressResponseBody(ResponseBody responseBody, OnProgressListener onProgressListener) {
    this.responseBody = responseBody;
    this.onProgressListener = onProgressListener;
  }

  @Override public MediaType contentType() {
    return responseBody.contentType();
  }

  @Override public long contentLength() throws IOException {
    return responseBody.contentLength();
  }

  @Override public BufferedSource source() throws IOException {
    if (bufferedSource == null) {
      bufferedSource = Okio.buffer(source(responseBody.source()));
    }
    return bufferedSource;
  }

  private Source source(Source source) {
    return new ForwardingSource(source) {
      long totalBytesRead = 0L;

      @Override public long read(Buffer sink, long byteCount) throws IOException {
        long bytesRead = super.read(sink, byteCount);
        // read() returns the number of bytes read, or -1 if this source is exhausted.
        totalBytesRead += bytesRead != -1 ? bytesRead : 0;
        onProgressListener.update(totalBytesRead, responseBody.contentLength(), bytesRead == -1);
        return bytesRead;
      }
    };
  }
}
